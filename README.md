# README #

These are the marco specific packages that are required to get a marco simulation running in gazebo.

Packages:  
- **marco_description**: the main urdf files that make up marco  
- **ant_description**: the models for the base are in here  
- **hit_gripper**: the models for the (by hit designed) gripper of the robot, along with the controller config & the gazebo models. The gripper control isnt implemented as of yet, but the model will load in.  
- **pmb2_description**: the model of the wheels is taken from here  
- **marco_gazebo**: Starts the gazebo environment and spawns marco inside using the marco_bringup package. Contains some additional worlds/models to load into the environment if desired.  
- **marco_bringup**: This package is used to spawn the marco model into gazebo.  
- **marco_controller_configuration_gazebo**: Loads up the correct controllers so that the robot can be controlled in gazebo.  

These packages are taken from the [old marco sim repo](https://bitbucket.org/hitcare/sim/src/master/src/), with some modifications made:  
- in marco_spawn.launch: comment out the joy_teleop.launch   
- in titanium_controllers.launch: commented out robot & camera arguments for spawning the move_group node.  
- in marco_gazebo.launch: changed the default of the *paused* argument from true to false, so that you can start planning right away.  